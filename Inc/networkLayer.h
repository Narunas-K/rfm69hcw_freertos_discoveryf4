#ifndef __NETWORKLAYER_H
#define __NETWORKLAYER_H


#include "main.h"


uint8_t *addIPv6header(uint16_t *sourceAddress, uint16_t *destinationAddress, uint16_t hopLimit, uint16_t payloadLenght, uint8_t *payload);

void decodeIPheader (uint8_t *IPpacket, uint16_t sourceIPaddress[], uint16_t destinationIPaddress[], uint16_t *payloadLength, uint16_t *hopLimit, uint8_t *IPver);

void  initARPtable(void);

void initRoutingTable(void);

bool routeExists(uint16_t *destinationIPaddress, uint8_t *EntryNumber);

void IPpacketSend(uint16_t *destinationIPaddress, uint8_t *payload, uint16_t payloadLength, uint16_t *sourceIPAddress, uint16_t hopLimit);

void sendRREQ(uint16_t *destinationIPaddress);

uint8_t *removeIPheader(uint8_t *IPpacket, uint16_t payloadLength);

void IPpacketReceived(bool acceptBroadCastPackets);

void extractRREQ(uint8_t *RREQmessage, uint8_t *flags, uint16_t *destinationIPaddr, uint8_t *hopCount, uint32_t *RREQID, 
										uint32_t *destinationSeqNo, uint16_t *originatorIPaddr, uint32_t *originatorSeqNo);
										
void proccessRREQ(uint8_t * RREQmessage, uint16_t *sourceIPfromIPheader, uint16_t hopLimit);

void emptyRoutingTableEntries( uint8_t *emptyEntriesNumber);

void UpdateRouteTableAfterRREQreceived(uint16_t *RREQoriginatorIPaddress, uint16_t *sourceIPfromIPheader, uint32_t RREQoriginatorSeqNo, uint8_t RREQflags, uint8_t RREQhopCount );

bool compareTwoIPAddresses(uint16_t *firstIPaddress, uint16_t *secondIPaddress);

uint8_t *generateRREP(uint16_t *destIP, uint16_t *originatorIP, uint32_t destSeqNo, bool requireACK,uint32_t routeLifeTime);

uint8_t resolveMACaddress(uint16_t *destinationIPaddress);

void generateRREPDestinationNode(uint16_t *destIP, uint16_t *originatorIP, uint32_t destSeqNo ,uint32_t routeLifeTime);

void extractRREP(uint8_t *RREPmessage, bool *ACKrequested, uint16_t *destIP, uint8_t *hopCount, uint32_t *destSeqNo, uint16_t *originatorIP, uint32_t *lifeTime);

void processRREP(uint8_t *RREPmessage, uint16_t *sourceIPfromHeader, uint16_t hopLimit);

uint8_t routingTableEntryNumToWhichEmptyEntryToWrite(void);

void processDatapacket(uint8_t *dataPacket, uint16_t dataLength, uint16_t *sourceIPfromIPheader, uint16_t *destinationIPfromIPheader, uint16_t hopLimit);

void updateRoutingTable(uint8_t entry, uint16_t *_destinationIPaddress, uint32_t _destinationSequenceNumber, uint8_t _flags, uint16_t *_nextHopAddress, uint8_t _hopCount, uint32_t _lifeTime, bool isDestSeqNoValid );

void initializeThisNodeIP(void);

void updateARPtable(uint16_t *sourceIP, uint8_t sourceMAC);

uint8_t *generatePSstatement(uint16_t *PSIPaddr);

uint8_t *generatePSreply(uint16_t *PSIPaddr);


uint16_t *decodePSstatementMsg(uint8_t *PSstatementMessage, uint8_t *decodedPSMAC);

uint16_t *decodePSreplytMsg(uint8_t *PSreplyMessage, uint8_t *decodedReplyPSMAC);

void BroadCastPSstatement(uint8_t *PSstatementPtr);

void processPSmessage (uint8_t *PSmessage);

void determinePrimaryStation(void);

uint32_t randTime(void);

void markRouteAsinvalid (uint8_t entry, bool isDestSeqNoValid);

void printPSinfo(void);

void node2Test(void);
#endif
