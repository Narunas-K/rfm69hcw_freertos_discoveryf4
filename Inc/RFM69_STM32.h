#ifndef __RFM69_STM32_H
#define __RFM69_STM32_H

#include "stm32f4xx_hal.h"
#include "main.h"
#include "stdbool.h"
#include "stdint.h"
#include "spi.h"





#define RF69_MAX_DATA_LEN       95 // to take advantage of the built in AES/CRC we want to limit the frame size to the internal FIFO size (66 bytes - 3 bytes overhead - 2 bytes crc)

#define CSMA_LIMIT              -90 // upper RX signal sensitivity threshold in dBm for carrier sense access
#define RF69_MODE_SLEEP         0 // XTAL OFF
#define RF69_MODE_STANDBY       1 // XTAL ON
#define RF69_MODE_SYNTH         2 // PLL ON
#define RF69_MODE_RX            3 // RX MODE
#define RF69_MODE_TX            4 // TX MODE

// available frequency bands
#define RF69_315MHZ            31 // non trivial values to avoid misconfiguration
#define RF69_433MHZ            43
#define RF69_868MHZ            86
#define RF69_915MHZ            91

#define null                  0
#define COURSE_TEMP_COEF    -90 // puts the temperature reading in the ballpark, user can fine tune the returned value
#define RF69_BROADCAST_ADDR 255
#define RF69_CSMA_LIMIT_MS 1000
#define RF69_TX_LIMIT_MS   1000
#define RF69_FSTEP  61.03515625 // == FXOSC / 2^19 = 32MHz / 2^19 (p13 in datasheet)

// TWS: define CTLbyte bits
#define RFM69_CTL_SENDACK   0x80
#define RFM69_CTL_REQACK    0x40

typedef struct{
		volatile uint8_t DATA[RF69_MAX_DATA_LEN]; // recv/xmit buf, including header & crc bytes
    volatile uint8_t DATALEN;
    volatile uint8_t SENDERID;
    volatile uint8_t TARGETID; // should match _address
    volatile uint8_t PAYLOADLEN;
    volatile uint8_t ACK_REQUESTED;
    volatile uint8_t ACK_RECEIVED; // should be polled immediately after sending a packet with ACK request
    volatile int16_t RSSI; // most accurate RSSI during reception (closest to the reception)
    volatile uint8_t _mode; // should be protected?
}RFM69;

typedef struct {
	uint8_t _mode;
	bool _promiscuousMode;
	uint8_t _powerLevel;
	bool _isRFM69HW;
	uint8_t _address;
}RFM69InitializeData;


extern RFM69 RFM69Data;
extern RFM69InitializeData RFM69InitData;

bool RFM69_Initialize( uint8_t freqBand, uint8_t nodeID, uint8_t networkID);

void RFM69_select(void);
void RFM69_unselect(void);

uint8_t RFM69_readReg(uint8_t addr);
void RFM69_writeReg(uint8_t addr, uint8_t value);

void RFM69_encrypt(const char* key);

void RFM69_setHighPowerRegs(bool onOff);
void RFM69_setHighPowerRegs(bool onOff);
void RFM69_setMode(uint8_t newMode);
void RFM69_setHighPower(bool onOff);
void RFM69_sleep(void);
void RFM69_setAddress(uint8_t addr);
void RFM69_setNetwork(uint8_t networkID);

int16_t RFM69_readRSSI(bool forceTrigger);
void RFM69_sendFrame(uint8_t toAddress, uint8_t* buffer, uint8_t bufferSize, bool requestACK, bool sendACK);
void RFM69_receiveBegin(void);
bool RFM69_receiveDone(void);
void RFM69_interruptHandler(void);
uint32_t millis(void);
void RFM69_promiscuous(bool onOff);
bool RFM69_ACKReceived(uint8_t fromNodeID);
bool RFM69_ACKRequested(void);
void RFM69_sendACK(uint8_t ACKToSend);

#endif
